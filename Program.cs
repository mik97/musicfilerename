﻿using System;
using System.IO;
using System.Windows;

namespace MusicFilesRename
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.Write("Folder path: ");
            var pathFolder = Console.ReadLine();
            if(Directory.Exists(pathFolder))
                ProcessDirectory(pathFolder);
            else
                Console.WriteLine("Folder not valid");
        }

        public static void ProcessDirectory(string targetDirectory)
        {
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
                ProcessFile(fileName);

            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
                ProcessDirectory(subdirectory);
        }

        public static void ProcessFile(string path)
        {
            FileInfo fi = new FileInfo(path);
            fi.MoveTo(destFileName: path.Replace("_-_", " ").Replace("_", " ").Replace("(Audio)", " ")
                .Replace("(256k)", "").Replace("(128k)", "").Replace("( 160kbps )", "").Replace("(Music Video)", "")
                .Replace("(Lyrics)", "").Replace("(Official Lyric Video)", "").Replace("(Official Video)", "").Trim());
            Console.WriteLine("Processed file '{0}'.", path);
        }
    }
}
